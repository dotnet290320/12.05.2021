﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp9
{
    public static class MyStringExtension
    {
        public static MyString ReplaceStartWith_(this MyString myString)
        {
            myString.TheString = myString.TheString.Replace(".", "*");
            return myString;
        }

        public static bool IsInt(this object obj)
        {
            bool is_int = obj is int;
            return is_int;
        }

        // Targil: 
        // 1
        // add to string class function:
        // - isUpper --> return true if all the characters are capital letters
        // 2
        // add to Int32 class function:
        // - isBiggerThan (int number) --> return true if the current int is bigger than number
        // 3
        // create class Ninja (sealed)
        // public string Name { get; set; }
        // -- create function Jump() inside ninja class which prints $"{Name} is jumping"
        // -- create function ThrowShurikon() OUTSIDE ninja class (i.e. NinjaExtension class )
        //         which prints $"{Name} is throwing a shurikon"
        // 4
        // [fluent]
        // create class MyNumber which holds an int
        // use fluent to make this lines to work:
        // MyNumber num = new MyNumber(10);
        // Console.WriteLine(num.plus(10).minus(20).multiple(3).power(2));






    }
}
