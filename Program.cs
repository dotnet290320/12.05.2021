﻿using System;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            MyString string1 = new MyString(" hello . . . ");

            string1.RemoveSpaces();
            string1.ReplaceDotWithStart();
            Console.WriteLine(string1);

            Console.WriteLine(string1.RemoveSpaces().ReplaceDotWithStart().ReplaceStartWith_());

            object o1 = 5;
            Console.WriteLine(o1.IsInt());

        }
    }
}
