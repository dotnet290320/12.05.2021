﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp9
{
    public class MyString
    {
        private string m_string;

        public string TheString
        {
            get
            {
                return m_string;
            }
            set
            {
                m_string = value;
            }
        }

        public MyString(string start_string)
        {
            m_string = start_string;
        }

        public MyString RemoveSpaces()
        {
            m_string = m_string.Trim();
            return this;
        }

        public MyString ReplaceDotWithStart()
        {
            m_string = m_string.Replace(".", "*");
            return this;
        }

        public override string ToString()
        {
            return m_string;
        }
    }
}
