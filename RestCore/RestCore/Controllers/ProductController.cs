﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestCore.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RestCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Product> GetProducts()
        {
            Debug.WriteLine(Request);
            return new Product[] {
                new Product() { Name = "Product #1" },
                new Product() { Name = "Product #2" },
};
        }
        [HttpGet("{id}")]
        public Product GetProduct()
        {
            return new Product()
            {
                ProductId = 1,
                Name = "Test Product"
            };
        }
    }
}
